\name{vdot}
\alias{vdot}
\title{Dot plots of single-cell expression of a gene}
\description{Dot plots of single-cell expression values of a gene stratified
by sample and cell types.}
\usage{
  vdot(sc,gene,ctype=NULL,cell_ctype=NULL,sampleid=NULL,
    sample_tags = NULL, bw = .5, s = .5, width=.95,
    xlim=NULL, ylim=NULL, xaxs="i",... )
}
\arguments{
  \item{sc}{Single-cell data object}
  \item{gene}{Gene name}
  \item{ctype}{Cell types to show, can be a subset of possible values. The
  order is used to order the panels in the plot.}
  \item{cell_ctype}{Types of the cells}
  \item{sampleid}{sample identifiers of the cells}
  \item{sample_tags}{custom sample labels (same lengths and order as the sample in the data)}
  \item{bw}{Bandwidth for smoothing the density of data points}
  \item{s}{Log-transformation shift}
  \item{width}{Width of the data points}
  \item{xlim}{Range of x-axis}
  \item{ylim}{Range of y-axis}
  \item{xaxs}{Type of x-axis range}
  \item{...}{options passed on to `plot.default`}
}

