ac_cat <- function(ac1,ac2,cg1=NULL,cg2=NULL,g=NULL)
{
  if( !all(rownames(ac1$N) == rownames(ac2$N)))
    stop("samples are different")

  if(is.null(g)) 
    g <- intersect( dimnames(ac1$y)[[3]],dimnames(ac2$y)[[3]])
  
  if(is.null(cg1))
    cg1 <- colnames(ac1$N)
  else
    cg1 <- intersect(cg1,colnames(ac1$N))
  
  if(is.null(cg2))
    cg2 <- colnames(ac2$N)
  else
    cg2 <- intersect(cg2,colnames(ac2$N))

  ac <- list()
  class(ac) <- "ac"
  ac$N <- cbind( ac1$N[,cg1],ac2$N[,cg2] )
  colnames(ac$N) <- c(cg1,cg2)
  ac$y <- c( ac1$y[,,g,cg1],ac2$y[,,g,cg2] )
  dim(ac$y) <- c(2,nrow(ac$N),length(g),ncol(ac$N))
  dimnames(ac$y) <- list( dimnames(ac1$y)[[1]],dimnames(ac1$y)[[2]],
    g,colnames(ac$N))
  ac$aggr_scale <- cbind( ac1$aggr_scale[,cg1],ac2$aggr_scale[,cg2])
  ac
}
