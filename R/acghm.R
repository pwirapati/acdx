acghm <- function(ac,g,o=NULL,oc=NULL,sat=1,interpolate=FALSE,
  snorm=TRUE,gnorm=TRUE,srt=0,loff=1.02,lambda=NULL)
{
  if(is.null(o)) o <- 1:dim(ac$y)[2]
  if(is.null(oc)) oc <- 1:dim(ac$y)[4]

  g <- intersect(g,dimnames(ac$y)[[3]])

  y <- aperm(ac$y[1,o,g,oc],c(1,3,2))
  sname <- sapply( dimnames(y)[[2]],function(k) paste(k,dimnames(y)[[1]]),
    USE.NAMES=F)
  dim(y) <- c(dim(y)[1]*dim(y)[2],dim(y)[3])
  dimnames(y) <- list(sname,g)
  if(snorm)
    y <- t(y/c(ac$aggr_scale[o,oc]))
  else
    y <- t(y)
  if(gnorm)
    y <- y/rowMeans(y,na.rm=T)*mean(y,na.rm=T)
  
  ng <- nrow(y)
  np <- ncol(y)

  if(!is.null(lambda)) y <- log(lambda+y)

  plot.new()
  plot.window(c(0,np),c(ng,0),xaxs="i",yaxs="i")
  rasterImage( as.raster( 1-tanh( sat*y )),0,ng,np,0,interpolate=interpolate)
  abline(v=length(o)*(0:length(oc)),col=2)
  text(xpd=NA,
    x=(-0.5+(1:length(oc)))*length(o),
    y=rep(loff*ng,length(oc)),
    labels=dimnames(ac$y)[[4]][oc],adj=1,srt=srt)
  invisible(y)
}
