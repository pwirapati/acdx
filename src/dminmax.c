#include <math.h>
#include <stdio.h>

void
dminmax(
  const int* dims, // ni, nj, nk, nH, nL
  const double* y, // [4] 2, ni, nj, nk
  const double* w, // sample weights
  const int* H,
  const int* L,
  const double *lambda_,
  double *dminmax // [nj] output
  )
{
  const int ni = dims[0], nj = dims[1], nk = dims[2], nH=dims[3], nL=dims[4];
  const double lambda = lambda_[0];

//fprintf(stderr,"ni = %d, nj = %d, nk = %d, nH = %d, nL = %d, lambda = %g\n",
//  ni, nj, nk, nH, nL, lambda );
  
#pragma omp parallel for schedule(runtime)
  for(int j = 0; j < nj; j++ )
    {
    const double *yj = y + j*2*ni;
    double minH = INFINITY;
    for(int h = 0; h < nH; h++ )
      {
      int k = H[h]-1;
      const double *yjk = yj + k*2*ni*nj;
      double Sw = 0, Swy = 0;
      for(int i = 0; i < ni; i++ )
        {
        if( !isnan(yjk[2*i]) )
          {
          Swy += w[i] * yjk[2*i];
          Sw += w[i];
          }
        }
      double mujk = Swy/Sw;
      if( mujk < minH ) minH = mujk;
      }
    double maxL = -INFINITY;
    for(int l = 0; l < nL; l++ )
      {
      int k = L[l]-1;
      const double *yjk = yj + k*2*ni*nj;
      double Sw = 0, Swy = 0;
      for(int i = 0; i < ni; i++ )
        {
        if( !isnan(yjk[2*i]) )
          {
          Swy += w[i] * yjk[2*i];
          Sw += w[i];
          }
        }
      double mujk = Swy/Sw;
      if( mujk > maxL ) maxL = mujk;
      }
    dminmax[j] = (lambda+minH)/(lambda+maxL);
    }
}

// mean difference in log(y+lambda) space
//
//
void
ldminmax(
  const int* dims, // ni, nj, nk, nH, nL
  const double* y, // [4] 2, ni, nj, nk
  const double* w, // sample weights
  const int* H,
  const int* L,
  const double *lambda_,
  double *ldminmax // [nj] output
  )
{
  const int ni = dims[0], nj = dims[1], nk = dims[2], nH=dims[3], nL=dims[4];
  const double lambda = lambda_[0];

//fprintf(stderr,"ni = %d, nj = %d, nk = %d, nH = %d, nL = %d, lambda = %g\n",
//  ni, nj, nk, nH, nL, lambda );
  
#pragma omp parallel for schedule(runtime)
  for(int j = 0; j < nj; j++ )
    {
    const double *yj = y + j*2*ni;
    double minH = INFINITY;
    for(int h = 0; h < nH; h++ )
      {
      int k = H[h]-1;
      const double *yjk = yj + k*2*ni*nj;
      double Sw = 0, Swy = 0;
      for(int i = 0; i < ni; i++ )
        {
        if( !isnan(yjk[2*i]) )
          {
          Swy += w[i] * log(yjk[2*i] + lambda);
          Sw += w[i];
          }
        }
      double mujk = Swy/Sw;
      if( mujk < minH ) minH = mujk;
      }
    double maxL = -INFINITY;
    for(int l = 0; l < nL; l++ )
      {
      int k = L[l]-1;
      const double *yjk = yj + k*2*ni*nj;
      double Sw = 0, Swy = 0;
      for(int i = 0; i < ni; i++ )
        {
        if( !isnan(yjk[2*i]) )
          {
          Swy += w[i] * log(yjk[2*i] + lambda);
          Sw += w[i];
          }
        }
      double mujk = Swy/Sw;
      if( mujk > maxL ) maxL = mujk;
      }
    ldminmax[j] = minH - maxL;
    }
}
